var express = require('express');
var router = express.Router();
var gcm = require('node-gcm');
var constants = require('./constants');
/* GET home page. */
router.get('/', function(req, res, next) {
  //res.render('index', { title: 'Express' });
  res.redirect(constants.serverURL + 'userlist');
});

/* GET Hello World page. */
router.post('/notify', function(req, res) {
	
	//var token = res.body.token;
   
    var message = new gcm.Message();
	 
	//API Server Key
	var sender = new gcm.Sender('AIzaSyBc0PICJnupjwjm3MJuTXZRl9GywnmcRqI');
	var registrationIds = [];
	 
	// Value the payload data to send...
	message.addData('message',req.body.replyText);
	message.addData('title','Notification' );  
	message.addData('msgcnt','3'); // Shows up in the notification in the status bar
	message.addData('soundname','beep.wav'); //Sound to play upon notification receipt - put in the www folder in app
	//message.collapseKey = 'demo';
	//message.delayWhileIdle = true; //Default is false
	//message.timeToLive = 3000;// Duration in seconds to hold in GCM and retry before timing out. Default 4 weeks (2,419,200 seconds) if not specified.
	 
	// At least one reg id required
	registrationIds.push(req.body.token);
	/**
	 * Parameters: message-literal, registrationIds-array, No. of retries, callback-function
	 */
	sender.send(message, registrationIds, 4, function (result) {
		res.redirect(constants.serverURL+ 'userlist');
	});
	
});

/* GET Userlist page. */
router.get('/userlist', function(req, res) {
    var db = req.db;
    //console.log(req);

    var collection = db.get('nissinDB');
	
    collection.findOne({$query: {}, $orderby: {$natural : -1}},function(e,docs){
        res.render('userlist', {
            "userdata" : docs
        });
    });
	
	
	});

/* GET New User page. */
router.get('/newuser', function(req, res) {
    res.render('newuser', { title: 'Add New User' });
});


/* POST to Add User Service */
router.post('/adduser', function(req, res) {

    // Set our internal DB variable
    var db = req.db;
    
    // Set our collection
    var collection = db.get('nissinDB');
	
    // Submit to the DB
    collection.insert({
        "date" : req.body.date,
		"areaName" : req.body.areaName,
		"totalCall" : req.body.totalCall,
		"productiveCall": req.body.productiveCall,
		"totalValue": req.body.totalValue,
		"mobileno": req.body.mobileno,
		"distrubitor": req.body.distrubitor,
		"reportingOfficer": req.body.reportingOfficer,
		"token": req.body.token
    }, function (err, doc) {
        if (err) {
            // If it failed, return error
            res.send("There was a problem adding the information to the database.");
			//ret = false;
        }
        else {
			
            // And forward to success page
            //res.redirect('http://192.168.2.5:3000/userlist');
			/* res.writeHead(301,{location:'http://192.168.2.5:3000/userlist'});
			res.end(); */
			//res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
			/*res.redirect('http://192.168.2.8:3000/userlist');*/
			res.send("Success.");
			
			}
		
    });
});


module.exports = router;