angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('userDataCtrl', function($scope, $stateParams, Chats, $http, $ionicPopup, $ionicLoading) {
  var vm = this;
  vm.submit = submit;
  var token = "";
  vm.date = "";
  var pushConfig = {
    android: {
      senderID: "297316142742"
    },
    ios: {
      alert: true,
      badge: true,
      sound: true
    }
  };

  var push = window.PushNotification.init(pushConfig);

  push.on('registration', function(data) {
    token = data.registrationId;
  });
  push.on('notification', function(data) {
      $ionicPopup.alert({
       title: "Push Notification",
       template: data.message
      }).then(function(){
        
      });

  });

  function submit(){
    
    if(((vm.date != "" && vm.date != undefined) && (vm.areaName != "" && vm.areaName != undefined) && (vm.totalCall != "" && vm.totalCall != undefined) && (vm.productiveCall != "" && vm.productiveCall != undefined) && (vm.totalValue != "" && vm.totalValue != undefined) && (vm.mobileno != "" && vm.mobileno != undefined) && (vm.distrubitor != "" && vm.distrubitor != undefined) && (vm.reportingOfficer != "" && vm.reportingOfficer != undefined))){
      var date = new Date(vm.date);
      var newDate = (date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
      $ionicLoading.show({
        template: '<ion-spinner icon="spiral"></ion-spinner>'
      });
      var data = {
        "date" : newDate,
        "areaName" : vm.areaName,
        "totalCall" : vm.totalCall,
        "productiveCall": vm.productiveCall,
        "totalValue": vm.totalValue,
        "mobileno": vm.mobileno,
        "distrubitor": vm.distrubitor,
        "reportingOfficer": vm.reportingOfficer,
        "token": token
      }

      var request = {
        method: 'POST',
        url: 'http://52.77.96.35:3000/adduser',
        headers: {
          'Content-Type': 'application/json'
        },
        data: data
      }

      $http(request).then(function(data){
        vm.date = "";
        vm.areaName = "";
        vm.totalCall = "";
        vm.productiveCall = "";
        vm.totalValue = "";
        vm.mobileno = "";
        vm.distrubitor = "";
        vm.reportingOfficer = "";
		$ionicLoading.hide();
        $ionicPopup.alert({
         title: "Notification",
         template: "Your Data Added Successfully"
        }).then(function(){
          console.log('added');
        });
      },function(error){
		$ionicLoading.hide();
        $ionicPopup.alert({
         title: "Notification",
         template: "Error in Adding"
        }).then(function(){
          console.log('Error');
        });
      });
    }else{
      $ionicPopup.alert({
       title: "Notification",
       template: "All Fields are Mandatory"
      }).then(function(){
        console.log('Mandatory');
      });
    }    
  }

})
.controller('AccountCtrl', function($scope) {
  var vm = this;
  vm.topRamen = topRamen;
  vm.scoopies = scoopies;
  vm.cupNoodels = cupNoodels;
  vm.hideRamen = false;
  vm.hideScoopies = false;
  vm.hideCupNoodels = false;

  function topRamen(){
    if(vm.hideRamen == false){
      vm.hideRamen = true;
      vm.hideScoopies = false;
      vm.hideCupNoodels = false;
    }else{
      vm.hideRamen = false;
      vm.hideScoopies = false;
      vm.hideCupNoodels = false;
    }
  }

  function scoopies(){
    if(vm.hideScoopies == false){
      vm.hideRamen = false;
      vm.hideScoopies = true;
      vm.hideCupNoodels = false;
    }else{
      vm.hideRamen = false;
      vm.hideScoopies = false;
      vm.hideCupNoodels = false;
    }
  }

  function cupNoodels(){
    if(vm.hideCupNoodels == false){
      vm.hideRamen = false;
      vm.hideScoopies = false;
      vm.hideCupNoodels = true;
    }else{
      vm.hideRamen = false;
      vm.hideScoopies = false;
      vm.hideCupNoodels = false;
    }
  }
});